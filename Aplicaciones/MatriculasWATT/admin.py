from django.contrib import admin
from .models import carreraWATT, cursoWATT, asignaturaWATT
# Register your models here.
admin.site.register(carreraWATT)
admin.site.register(cursoWATT)
admin.site.register(asignaturaWATT)